﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Colours_Json
{
    class ColourNames
    {
        // Name of colour
        public string name { get; set; }
        // Hexcode for colour
        public string hexcode { get; set; }
    }
}
