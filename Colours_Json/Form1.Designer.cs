﻿namespace Colours_Json
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tb_nameInput = new System.Windows.Forms.TextBox();
            this.btn_addColour = new System.Windows.Forms.Button();
            this.rtb_colourJson = new System.Windows.Forms.RichTextBox();
            this.lbl_KeyCount = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // tb_nameInput
            // 
            this.tb_nameInput.Location = new System.Drawing.Point(12, 269);
            this.tb_nameInput.Name = "tb_nameInput";
            this.tb_nameInput.Size = new System.Drawing.Size(286, 20);
            this.tb_nameInput.TabIndex = 6;
            this.tb_nameInput.TextChanged += new System.EventHandler(this.tb_nameInput_TextChanged);
            // 
            // btn_addColour
            // 
            this.btn_addColour.Location = new System.Drawing.Point(12, 295);
            this.btn_addColour.Name = "btn_addColour";
            this.btn_addColour.Size = new System.Drawing.Size(286, 23);
            this.btn_addColour.TabIndex = 5;
            this.btn_addColour.Text = "Add Colour";
            this.btn_addColour.UseVisualStyleBackColor = true;
            this.btn_addColour.Click += new System.EventHandler(this.btn_addColour_Click);
            // 
            // rtb_colourJson
            // 
            this.rtb_colourJson.Location = new System.Drawing.Point(12, 12);
            this.rtb_colourJson.Name = "rtb_colourJson";
            this.rtb_colourJson.Size = new System.Drawing.Size(286, 251);
            this.rtb_colourJson.TabIndex = 4;
            this.rtb_colourJson.Text = "";
            // 
            // lbl_KeyCount
            // 
            this.lbl_KeyCount.AutoSize = true;
            this.lbl_KeyCount.Location = new System.Drawing.Point(12, 321);
            this.lbl_KeyCount.Name = "lbl_KeyCount";
            this.lbl_KeyCount.Size = new System.Drawing.Size(56, 13);
            this.lbl_KeyCount.TabIndex = 7;
            this.lbl_KeyCount.Text = "Key Count";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(310, 345);
            this.Controls.Add(this.lbl_KeyCount);
            this.Controls.Add(this.tb_nameInput);
            this.Controls.Add(this.btn_addColour);
            this.Controls.Add(this.rtb_colourJson);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tb_nameInput;
        private System.Windows.Forms.Button btn_addColour;
        private System.Windows.Forms.RichTextBox rtb_colourJson;
        private System.Windows.Forms.Label lbl_KeyCount;
    }
}

