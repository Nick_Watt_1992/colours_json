﻿/*
 * Author: Nicholas Watt
 * ID: J221822
 * Assessment 3: Multi-process application and third party library
 */

using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Colours_Json
{
    public partial class Form1 : Form
    {
        //All colours
        List<ColourNames> allColours = new List<ColourNames>();

        //Key Count for colour name textbox
        private int keyCount = 0;

        public Form1()
        {
            InitializeComponent();

            displayJson();
        }

        //Opens the colour picker to allow for the user to select a colour
        private void btn_addColour_Click(object sender, EventArgs e)
        {
            //New colour dialog instance
            ColorDialog colourDlg = new ColorDialog();

            //
            colourDlg.AnyColor = true;
            colourDlg.SolidColorOnly = false;
            colourDlg.Color = Color.White;

            //The hexcode from the colour picker is stored here
            string hexcode = "";

            //If a colour is chosen and the textbox is not empty then create new colour
            if (colourDlg.ShowDialog() == DialogResult.OK && !string.IsNullOrWhiteSpace(tb_nameInput.Text))
            {

                hexcode = "#" + (colourDlg.Color.ToArgb() & 0x00FFFFFF).ToString("X6");

                //Creates new colour
                ColourNames temp = new ColourNames
                {
                    name = tb_nameInput.Text,
                    hexcode = hexcode
                };

                allColours.Add(temp);
            }
            //If a colour is chosen and the textbox is empty then prompt user
            else
            {
                MessageBox.Show("Please enter a name before selecting a colour");
            }
            SerializeAllColours();
            displayJson();
        }

        //Converts from json to display data to the user
        private void displayJson()
        {
            string output = JsonConvert.SerializeObject(allColours);
            rtb_colourJson.Text = output;
        }

        //Serializes allColours to colours.json
        private void SerializeAllColours()
        {
            //
            using (StreamWriter file = File.CreateText(Application.StartupPath + @"\colours.json"))
            {
                JsonSerializer serializer = new JsonSerializer();
                serializer.Serialize(file, allColours);
            }
        }

        //Deserializes allColours from colours.json
        private void DeserializeAllColours()
        {
            string jsonString = File.ReadAllText(Application.StartupPath + @"\colours.json");
            allColours = JsonConvert.DeserializeObject<List<ColourNames>>(jsonString);
        }

        // Count all the keystrokes entered into any textbox
        private void countKeystrokes()
        {
            keyCount++;
            lbl_KeyCount.Text = "Number of keystrokes = " + keyCount.ToString();
        }

        //When textbox input changes, count keys
        private void tb_nameInput_TextChanged(object sender, EventArgs e)
        {
            countKeystrokes();
        }
    }
}
