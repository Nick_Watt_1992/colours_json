﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Colours_Json
{
    public class KeyCount
    {
        static long keyPressCount = 0;
        static long backspacePressed = 0;
        static long returnPressed = 0;
        static long escPressed = 0;
        public string output = backspacePressed + " backspaces pressed\r\n" +
                escPressed + " escapes pressed\r\n" +
                returnPressed + " returns pressed\r\n" +
                keyPressCount + " other keys pressed\r\n";
        public void myKeyCounter(object sender, KeyPressEventArgs ex)
        {
            switch (ex.KeyChar)
            {
                // Counts the backspaces.
                case '\b':
                    backspacePressed = backspacePressed + 1;
                    break;
                // Counts the ENTER keys.
                case '\r':
                    returnPressed = returnPressed + 1;
                    break;
                // Counts the ESC keys.  
                case (char)27:
                    escPressed = escPressed + 1;
                    break;
                // Counts all other keys.
                default:
                    keyPressCount = keyPressCount + 1;
                    break;
            }

            output =
                backspacePressed + " backspaces pressed\n" +
                escPressed + " escapes pressed\n" +
                returnPressed + " returns pressed\n" +
                keyPressCount + " other keys pressed\n";
            ex.Handled = true;
        }
    }
}
